using System;
using Chuah.UrlShortening.Api.Database;
using Chuah.UrlShortening.Api.Exceptions;
using Chuah.UrlShortening.Api.Logics;
using Xunit;

namespace Chuah.UrlShortening.Api.UnitTests.Logics
{
    public class UrlShorteningServiceTest
    {
        private readonly IUrlShorteningService _urlShorteningService;

        public UrlShorteningServiceTest()
        {
            _urlShorteningService = new UrlShorteningService(new InMemoryDatabase());
        }

        [Fact]
        public void Shorten_WithNullUri_ThrowArgumentNullException()
        {
            // Arrange
            var uri = (Uri)null;

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                _urlShorteningService.Shorten(uri);
            });
        }

        [Fact]
        public void Shorten_WithWindowsFilePath_ThrowArgumentException()
        {
            // Arrange
            var uri = new Uri(@"C:\gitlab");

            // Act & Assert
            Assert.Throws<ArgumentException>(() =>
            {
                _urlShorteningService.Shorten(uri);
            });
        }

        [Fact]
        public void Shorten_WithValidHttpUrl_ReturnsShortenUrl()
        {
            // Arrange
            var uri = new Uri("http://www.gitlab.com");

            // Act
            var result = _urlShorteningService.Shorten(uri);

            // Assert
            Assert.NotNull(result);
            Assert.StartsWith(UrlShorteningService.HOST, result);
            Assert.True(result.Length == UrlShorteningService.HOST.Length + 8);
        }

        [Fact]
        public void Shorten_WithValidHttpsUrl_ReturnsShortenUrl()
        {
            // Arrange
            var uri = new Uri("https://www.gitlab.com");

            // Act
            var result = _urlShorteningService.Shorten(uri);

            // Assert
            Assert.NotNull(result);
            Assert.StartsWith(UrlShorteningService.HOST, result);
            Assert.True(result.Length == UrlShorteningService.HOST.Length + 8);
        }

        [Fact]
        public void Shorten_WithRepeatedUrl_ReturnsShortenUrlAsBefore()
        {
            // Arrange
            var uri = new Uri("https://www.gitlab.com");

            // Act
            var result = _urlShorteningService.Shorten(uri);

            // Assert
            Assert.NotNull(result);
            Assert.StartsWith(UrlShorteningService.HOST, result);
            Assert.True(result.Length == UrlShorteningService.HOST.Length + 8);

            // Act 2
            result = _urlShorteningService.Shorten(uri);

            // Assert 2
            Assert.NotNull(result);
            Assert.StartsWith(UrlShorteningService.HOST, result);
            Assert.True(result.Length == UrlShorteningService.HOST.Length + 8);
        }

        [Fact]
        public void Flatten_WithNullUri_ThrowArgumentNullException()
        {
            // Arrange
            var uri = (Uri)null;

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                _urlShorteningService.Flatten(uri);
            });
        }

        [Fact]
        public void Flatten_WithWindowsFilePath_ThrowArgumentException()
        {
            // Arrange
            var uri = new Uri(@"C:\gitlab");

            // Act & Assert
            Assert.Throws<ArgumentException>(() =>
            {
                _urlShorteningService.Flatten(uri);
            });
        }

        [Fact]
        public void Flatten_WithValidShortenUrl_ReturnsOriUrl()
        {
            // Arrange
            var originalUri = new Uri("https://www.gitlab.com");
            var shortenUri = _urlShorteningService.Shorten(new Uri(originalUri.AbsoluteUri));

            // Act
            var result = _urlShorteningService.Flatten(new Uri(shortenUri));

            // Assert
            Assert.NotNull(result);
            Assert.Equal(originalUri.AbsoluteUri, result);
        }

        [Fact]
        public void Flatten_WithLongUrl_ThrowsNotAShortenUrlException()
        {
            // Arrange
            var longUri = new Uri("https://www.gitlab.com");

            // Act & Assert
            Assert.Throws<NotAShortenUrlException>(() =>
            {
                var result = _urlShorteningService.Flatten(longUri);
            });
        }
    }
}
