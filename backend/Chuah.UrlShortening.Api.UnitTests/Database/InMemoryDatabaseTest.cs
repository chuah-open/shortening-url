﻿using System;
using Chuah.UrlShortening.Api.Database;
using Xunit;

namespace Chuah.UrlShortening.Api.UnitTests.Database
{
    public class InMemoryDatabaseTest
    {
        private readonly IInMemoryDatabase _database;

        public InMemoryDatabaseTest()
        {
            _database = new InMemoryDatabase();
        }

        [Fact]
        public void Add_FirstTime()
        {
            // Arrange
            var originalUrl = "https://www.microsoft.com";
            var shortenUrl = "https://chuah.ly/abCdefg0";
            _database.Add(originalUrl, shortenUrl);

            // Act
            var result = _database.GetOriginalUrl(shortenUrl);

            // Assert
            Assert.Equal(originalUrl, result);
        }

        [Fact]
        public void GetOriginalUrl_WithNullUrl_ReturnsNull()
        {
            // Arrange
            var originalUrl = "https://www.microsoft.com";
            var shortenUrl = "https://chuah.ly/abCdefg0";
            _database.Add(originalUrl, shortenUrl);

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() =>
            {
                _database.GetOriginalUrl(null);
            });
        }

        [Fact]
        public void GetOriginalUrl_WithEmptyUrl_ReturnsNull()
        {
            // Arrange
            var originalUrl = "https://www.microsoft.com";
            var shortenUrl = "https://chuah.ly/abCdefg0";
            _database.Add(originalUrl, shortenUrl);

            // Act
            var result = _database.GetOriginalUrl(string.Empty);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public void GetOriginalUrl_WithWhitespaceUrl_ReturnsNull()
        {
            // Arrange
            var originalUrl = "https://www.microsoft.com";
            var shortenUrl = "https://chuah.ly/abCdefg0";
            _database.Add(originalUrl, shortenUrl);

            // Act
            var result = _database.GetOriginalUrl("    ");

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public void GetOriginalUrl_WithNonExistShortenUrl_ReturnsNull()
        {
            // Arrange
            var originalUrl = "https://www.microsoft.com";
            var shortenUrl = "https://chuah.ly/abCdefg0";
            _database.Add(originalUrl, shortenUrl);

            // Act
            var result = _database.GetOriginalUrl("https://chuah.ly/xxYYzz99");

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public void GetShortenUrl_WithNonExistUrl_ReturnsNull()
        {
            // Arrange


            // Act
            var result = _database.GetShortenUrl("https://www.gitlab.com");

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public void GetShortenUrl_WithExistedUrl_ReturnsShortenUrl()
        {
            // Arrange
            var originalUrl = "https://www.microsoft.com";
            var shortenUrl = "https://chuah.ly/abCdefg0";
            _database.Add(originalUrl, shortenUrl);

            // Act
            var result = _database.GetShortenUrl(originalUrl);

            // Assert
            Assert.Equal(shortenUrl, result);
        }

        [Fact]
        public void GetShortenUrl_WithRepeatedUrl_ReturnsSameUrlAsBefore()
        {
            // Arrange
            var originalUrl = "https://www.microsoft.com";
            var shortenUrl = "https://chuah.ly/abCdefg0";
            _database.Add(originalUrl, shortenUrl);

            // Act
            var result = _database.GetShortenUrl(originalUrl);

            // Assert
            Assert.Equal(shortenUrl, result);

            // Act 2
            result = _database.GetShortenUrl(originalUrl);

            // Assert 2
            Assert.Equal(shortenUrl, result);
        }

        [Fact]
        public void ShortenUrlExists_WithExistedUrl_ReturnsTrue()
        {
            // Arrange
            var originalUrl = "https://www.microsoft.com";
            var shortenUrl = "https://chuah.ly/abCdefg0";
            _database.Add(originalUrl, shortenUrl);

            // Act
            var result = _database.ShortenUrlExists(shortenUrl);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void ShortenUrlExists_WithNoneExistedUrl_ReturnsFalse()
        {
            // Arrange
            var originalUrl = "https://www.microsoft.com";
            var shortenUrl = "https://chuah.ly/abCdefg0";
            _database.Add(originalUrl, shortenUrl);

            // Act
            var result = _database.ShortenUrlExists("https://chuah.ly/1234567B");

            // Assert
            Assert.False(result);
        }
    }
}
