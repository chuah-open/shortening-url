﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chuah.UrlShortening.Api.Constants;
using Chuah.UrlShortening.Api.Exceptions;
using Chuah.UrlShortening.Api.Middlewares;
using Microsoft.AspNetCore.Http;
using Xunit;

namespace Chuah.UrlShortening.Api.UnitTests.Middlewares
{
    public class ExceptionMiddlewareTest
    {
        [Fact]
        public async Task Invoke_Status400BadRequest()
        {
            // Arrange
            // The mockNextMiddleware mock out the real world middleware that invoke a controller's method.
            var mockLogger = LoggerHelper.MockLogger<ExceptionMiddleware>();

            RequestDelegate mockNextMiddleware = (HttpContext) =>
            {
                return Task.FromException(new ApiRequestException(Errors.INVALID_URL));
            };
            var httpContext = new DefaultHttpContext();
            var middleware = new ExceptionMiddleware(mockNextMiddleware, mockLogger.Object);

            // Act
            await middleware.InvokeAsync(httpContext);

            // Assert
            Assert.Equal(StatusCodes.Status400BadRequest, httpContext.Response.StatusCode);
        }

        [Fact]
        public async Task Invoke_Status500InternalServerError()
        {
            // Arrange
            // The mockNextMiddleware mock out the real world middleware that invoke a controller's method.
            var mockLogger = LoggerHelper.MockLogger<ExceptionMiddleware>();

            RequestDelegate mockNextMiddleware = (HttpContext) =>
            {
                return Task.FromException(new Exception("Some unhandled exception"));
            };
            var httpContext = new DefaultHttpContext();
            var middleware = new ExceptionMiddleware(mockNextMiddleware, mockLogger.Object);

            // Act
            await middleware.InvokeAsync(httpContext);

            // Assert
            Assert.Equal(StatusCodes.Status500InternalServerError, httpContext.Response.StatusCode);
        }

        [Fact]
        public async Task Invoke_Status200OK()
        {
            // Arrange
            // The mockNextMiddleware mock out the real world middleware that invoke a controller's method.
            var mockLogger = LoggerHelper.MockLogger<ExceptionMiddleware>();

            RequestDelegate mockNextMiddleware = (HttpContext) =>
            {
                return Task.CompletedTask;
            };
            var httpContext = new DefaultHttpContext();
            var middleware = new ExceptionMiddleware(mockNextMiddleware, mockLogger.Object);

            // Act
            await middleware.InvokeAsync(httpContext);

            // Assert
            Assert.Equal(StatusCodes.Status200OK, httpContext.Response.StatusCode);
        }
    }
}
