:: Delete folders recursively without any prompt
rmdir .\TestResults /s /q
rmdir .\TestCoverageReport /s /q

:: Run unit test and collect code coverage data
dotnet test --collect:"XPlat Code Coverage"

:: Convert the code coverage data to Html format
reportgenerator -reports:"TestResults\*\coverage.cobertura.xml" -targetdir:.\TestCoverageReport -reporttypes:Html