﻿﻿﻿

# About

This is a Unit Test project that test the code in the `Chuah.UrlShortening.Api` project. The tools that this project uses are:

| Tool                                                         | Purposes                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [xUnit.net](https://xunit.net/)                              | A unit testing tool.                                         |
| [Moq](https://github.com/Moq/moq)                            | A mock framework that is used to mock out data and logic in unit testing context. |
| [Coverlet](https://github.com/coverlet-coverage/coverlet)    | A cross platform tool that collects the unit test coverage data. |
| [ReportGenerator](https://github.com/danielpalme/ReportGenerator) | A tool that converts the coverage report to various formats. For example, it reads the coverage report produced by `Coverlet` and generate a HTML report. |



# How to Run

Build and use the **Test Explorer** that comes with Visual Studio to run the tests. I have prepared a handy batch file `generate_coverage_report.cmd` which you can double click it which then it will build, test and generate a test coverage report. The output path of the test coverage report is `{unit-test-project-path}\TestCoverageReport`. After the report is generated, simply browse to the `TestCoverageReport` directory and open the `index.html` file to see the test coverage result.



# References

- [Test controller logic in ASP.NET Core | Microsoft Docs](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/testing?ranMID=43674&ranEAID=rl2xnKiLcHs&ranSiteID=rl2xnKiLcHs-LUam2oL7H3HfpqXDl7qflw&epi=rl2xnKiLcHs-LUam2oL7H3HfpqXDl7qflw&irgwc=1&OCID=AID2200057_aff_7795_1243925&tduid=(ir__n9tukbrb3kkf6i0wlnyvcfpb6n2xo9nwyeqcadfp00)(7795)(1243925)(rl2xnKiLcHs-LUam2oL7H3HfpqXDl7qflw)()&irclickid=_n9tukbrb3kkf6i0wlnyvcfpb6n2xo9nwyeqcadfp00&view=aspnetcore-6.0)
- [How to Exclude Code From Code Coverage - DZone Web Dev](https://dzone.com/articles/how-to-exclude-code-from-code-coverage)
- [coverlet/VSTestIntegration.md at master · coverlet-coverage/coverlet · GitHub](https://github.com/coverlet-coverage/coverlet/blob/master/Documentation/VSTestIntegration.md)
- [ASP.NET Core - How to unit test your middleware class (makolyte.com)](https://makolyte.com/aspdotnet-how-to-unit-test-your-middleware-class/)
- [The Code Blogger - How to Unit Test ASP .NET Core Middleware ?](https://thecodeblogger.com/2020/10/11/how-to-unit-test-asp-net-core-middleware/)
- [c# - How do you mock ILogger LogInformation - Stack Overflow](https://stackoverflow.com/questions/52707702/how-do-you-mock-ilogger-loginformation)