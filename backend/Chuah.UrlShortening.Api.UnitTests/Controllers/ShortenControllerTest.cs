﻿using System;
using Chuah.UrlShortening.Api.Constants;
using Chuah.UrlShortening.Api.Controllers;
using Chuah.UrlShortening.Api.Exceptions;
using Chuah.UrlShortening.Api.Logics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Chuah.UrlShortening.Api.UnitTests.Controllers
{
    public class ShortenControllerTest
    {
        private readonly Mock<IUrlShorteningService> _urlShorteningService;

        public ShortenControllerTest()
        {
            _urlShorteningService = new Mock<IUrlShorteningService>();
        }

        [Fact]
        public void Post_WithEmptyUrl_ThrowsApiRequestException()
        {
            // Arrange
            var controller = new ShortenController(_urlShorteningService.Object);

            // Act
            ApiRequestException exception = null;
            try
            {
                controller.Post(new ShortenInputDto()
                {
                    Url = string.Empty
                });
            }
            catch (Exception ex)
            {
                exception = ex as ApiRequestException;
            }

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(Errors.INVALID_URL, exception.Message);
        }

        [Fact]
        public void Post_WithWhitespaceOnlyUrl_ThrowsApiRequestException()
        {
            // Arrange
            var controller = new ShortenController(_urlShorteningService.Object);

            // Act
            ApiRequestException exception = null;
            try
            {
                controller.Post(new ShortenInputDto()
                {
                    Url = "     "
                });
            }
            catch (Exception ex)
            {
                exception = ex as ApiRequestException;
            }

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(Errors.INVALID_URL, exception.Message);
        }

        [Fact]
        public void Post_WithWindowsFilePath_ThrowsApiRequestException()
        {
            // Arrange
            var controller = new ShortenController(_urlShorteningService.Object);

            // Act
            ApiRequestException exception = null;
            try
            {
                controller.Post(new ShortenInputDto()
                {
                    Url = @"C:\Windows"
                });
            }
            catch (Exception ex)
            {
                exception = ex as ApiRequestException;
            }

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(Errors.INVALID_URL, exception.Message);
        }

        [Fact]
        public void Post_WithWellFormedHttpUrl_ReturnsStatus200OK()
        {
            // Arrange
            _urlShorteningService
                .Setup(x => x.Shorten(It.IsAny<Uri>()))
                .Returns(() => $"{UrlShorteningService.HOST}aAbBcCd5");

            var controller = new ShortenController(_urlShorteningService.Object);

            // Act
            var result = controller.Post(new ShortenInputDto()
            {
                Url = "http://www.gitlab.com"
            }) as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status200OK, result.StatusCode);
            var responseDto = result.Value as ShortenResponseDto;
            Assert.StartsWith(UrlShorteningService.HOST, responseDto.ShortUrl);
        }

        [Fact]
        public void Post_WithWellFormedHttpsUrl_ReturnsStatus200OK()
        {
            // Arrange
            _urlShorteningService
                .Setup(x => x.Shorten(It.IsAny<Uri>()))
                .Returns(() => $"{UrlShorteningService.HOST}aAbBcCd5");

            var controller = new ShortenController(_urlShorteningService.Object);

            // Act
            var result = controller.Post(new ShortenInputDto()
            {
                Url = "https://www.gitlab.com"
            }) as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status200OK, result.StatusCode);
            var responseDto = result.Value as ShortenResponseDto;
            Assert.StartsWith(UrlShorteningService.HOST, responseDto.ShortUrl);
        }

        [Fact]
        public void Post_WithRepetitiveUrl_ThrowsApiRequestException()
        {
            // Arrange
            _urlShorteningService
                .Setup(x => x.Shorten(It.IsAny<Uri>()))
                .Throws<UrlAlreadyShortenException>();

            var controller = new ShortenController(_urlShorteningService.Object);

            // Act
            ApiRequestException apiException = null;
            try
            {
                controller.Post(new ShortenInputDto()
                {
                    Url = "http://www.gitlab.com"
                });
            }
            catch (Exception ex)
            {
                apiException = ex as ApiRequestException;
            }

            // Assert
            Assert.NotNull(apiException);
            Assert.Equal(Errors.ALREADY_A_SHORTEN_URL, apiException.Message);
        }
    }
}
