﻿using System;
using Chuah.UrlShortening.Api.Constants;
using Chuah.UrlShortening.Api.Controllers;
using Chuah.UrlShortening.Api.Exceptions;
using Chuah.UrlShortening.Api.Logics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Chuah.UrlShortening.Api.UnitTests.Controllers
{
    public class FlattenControllerTest
    {
        private readonly Mock<IUrlShorteningService> _urlShorteningService;

        public FlattenControllerTest()
        {
            _urlShorteningService = new Mock<IUrlShorteningService>();
        }

        [Fact]
        public void Post_WithEmptyUrl_ThrowsApiRequestException()
        {
            // Arrange
            var controller = new FlattenController(_urlShorteningService.Object);

            // Act
            ApiRequestException exception = null;
            try
            {
                controller.Post(new FlattenInputDto()
                {
                    Url = string.Empty
                });
            }
            catch (Exception ex)
            {
                exception = ex as ApiRequestException;
            }

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(Errors.INVALID_URL, exception.Message);
        }

        [Fact]
        public void Post_WithWhitespaceOnlyUrl_ThrowsApiRequestException()
        {
            // Arrange
            var controller = new FlattenController(_urlShorteningService.Object);

            // Act
            ApiRequestException exception = null;
            try
            {
                controller.Post(new FlattenInputDto()
                {
                    Url = "     "
                });
            }
            catch (Exception ex)
            {
                exception = ex as ApiRequestException;
            }

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(Errors.INVALID_URL, exception.Message);
        }

        [Fact]
        public void Post_WithWindowsFilePath_ThrowsApiRequestException()
        {
            // Arrange
            var controller = new FlattenController(_urlShorteningService.Object);

            // Act
            ApiRequestException exception = null;
            try
            {
                controller.Post(new FlattenInputDto()
                {
                    Url = @"C:\Windows"
                });
            }
            catch (Exception ex)
            {
                exception = ex as ApiRequestException;
            }

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(Errors.INVALID_URL, exception.Message);
        }

        [Fact]
        public void Post_WithWellFormedHttpUrl_ReturnsStatus200OK()
        {
            // Arrange
            var originalUrl = "http://www.gitlab.com";
            var shortenUrl = $"{UrlShorteningService.HOST}aabbccD7";
            _urlShorteningService
                .Setup(x => x.Flatten(new Uri(shortenUrl)))
                .Returns(() => originalUrl);

            var controller = new FlattenController(_urlShorteningService.Object);

            // Act
            var result = controller.Post(new FlattenInputDto()
            {
                Url = shortenUrl
            }) as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status200OK, result.StatusCode);
            var responseDto = result.Value as FlattenResponseDto;
            Assert.Equal(originalUrl, responseDto.OriginalUrl);
            Assert.StartsWith(UrlShorteningService.HOST, responseDto.ShortUrl);
        }

        [Fact]
        public void Post_WithWellFormedHttpsUrl_ReturnsStatus200OK()
        {
            // Arrange
            var originalUrl = "https://www.gitlab.com";
            var shortenUrl = $"{UrlShorteningService.HOST}aabbccD7";
            _urlShorteningService
                .Setup(x => x.Flatten(new Uri(shortenUrl)))
                .Returns(() => originalUrl);

            var controller = new FlattenController(_urlShorteningService.Object);

            // Act
            var result = controller.Post(new FlattenInputDto()
            {
                Url = shortenUrl
            }) as OkObjectResult;

            // Assert
            Assert.NotNull(result);
            Assert.Equal(StatusCodes.Status200OK, result.StatusCode);
            var responseDto = result.Value as FlattenResponseDto;
            Assert.Equal(originalUrl, responseDto.OriginalUrl);
            Assert.StartsWith(UrlShorteningService.HOST, responseDto.ShortUrl);
        }

        [Fact]
        public void Post_WithAlreadyShortenUrl_ThrowsApiRequestException()
        {
            // Arrange
            var shortenUrl = $"{UrlShorteningService.HOST}aabbccD7";
            _urlShorteningService
                .Setup(x => x.Flatten(new Uri(shortenUrl)))
                .Throws<NotAShortenUrlException>();

            var controller = new FlattenController(_urlShorteningService.Object);

            // Act
            ApiRequestException apiException = null;
            try
            {
                controller.Post(new FlattenInputDto()
                {
                    Url = shortenUrl
                });
            }
            catch (Exception ex)
            {
                apiException = ex as ApiRequestException;
            }

            // Assert
            Assert.NotNull(apiException);
            Assert.Equal(Errors.NOT_A_SHORTEN_URL, apiException.Message);
        }

        [Fact]
        public void Post_WithAlreadyShortenUrl2_ThrowsApiRequestException()
        {
            // -----------------------------------------------------------------------------------------------------------------
            // NOTE:
            // This unit test method is not meaningful. It is added here just to make the Branch Coverage (Code Coverage)
            // looks nice which is 100%. In fact, we can never achieve 100% because the
            // 'already shorten URL' is always a 'https' path. Therefore, the condition inside
            // the Post method:
            //
            //      (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps)
            //
            // where the 'uriResult.Scheme == Uri.UriSchemeHttp' can never be hit.
            //
            // Comment out this unit test method will gain only 83.3% Branch Coverage.
            //
            // The solution is to rewrite the logic so that we can achieve 100% Branch Coverage
            // but this violates the good practice where we should not modify the code to
            // accomodate unit test.
            //
            // What we have learned from here is that, sometimes code coverage is just not possible
            // to achieve 100% and it is perfectly acceptable if all the test cases have been covered.
            // -----------------------------------------------------------------------------------------------------------------

            // Arrange
            // Note that here we change the shorten URL to http just to achive the 100% Branch Coverage.
            // In real world, the shorten URL is always started with 'https' because this is what is returned
            // from our API.
            var shortenUrl = $"{UrlShorteningService.HOST}aabbccD7".Replace("https", "http");
            _urlShorteningService
                .Setup(x => x.Flatten(new Uri(shortenUrl)))
                .Throws<NotAShortenUrlException>();
            var controller = new FlattenController(_urlShorteningService.Object);

            // Act
            ApiRequestException apiException = null;
            try
            {
                controller.Post(new FlattenInputDto()
                {
                    Url = shortenUrl
                });
            }
            catch (Exception ex)
            {
                apiException = ex as ApiRequestException;
            }

            // Assert
            Assert.NotNull(apiException);
            Assert.Equal(Errors.NOT_A_SHORTEN_URL, apiException.Message);
        }
    }
}
