using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Chuah.UrlShortening.Api.Database;
using Chuah.UrlShortening.Api.Logics;
using Chuah.UrlShortening.Api.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Chuah.UrlShortening.Api
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Chuah.UrlShortening.Api", Version = "v1" });

                // The xml file must be set to 'Copy always', otherwise the Swagger UI cannot be rendered
                // in Azure App Service.
                var filePath = Path.Combine(AppContext.BaseDirectory, "Chuah.UrlShortening.Api.xml");
                c.IncludeXmlComments(filePath, includeControllerXmlComments: true);
            });

            services.AddSingleton<IInMemoryDatabase, InMemoryDatabase>();
            services.AddTransient<IUrlShorteningService, UrlShorteningService>();

            // Configure to enforce all the endpoint URLs to become lower case.
            services.Configure<RouteOptions>(config => config.LowercaseUrls = true);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Put the two lines below in the if clause above to not render the swagger UI in production.
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Chuah.UrlShortening.Api v1"));

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
