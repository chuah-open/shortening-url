﻿using System.Diagnostics.CodeAnalysis;

namespace Chuah.UrlShortening.Api.Controllers
{
    [ExcludeFromCodeCoverage]
    public class FlattenResponseDto
    {
        public string OriginalUrl { get; set; }

        public string ShortUrl { get; set; }        
    }
}
