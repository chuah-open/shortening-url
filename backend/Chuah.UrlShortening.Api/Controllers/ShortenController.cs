﻿using System;
using Chuah.UrlShortening.Api.Constants;
using Chuah.UrlShortening.Api.Exceptions;
using Chuah.UrlShortening.Api.Logics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Chuah.UrlShortening.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShortenController : ControllerBase
    {
        private readonly IUrlShorteningService _urlShorteningService;

        public ShortenController(IUrlShorteningService urlShorteningService)
        {
            _urlShorteningService = urlShorteningService;
        }

        /// <summary>
        /// Shortening an URL.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(ShortenResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDto), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponseDto), StatusCodes.Status500InternalServerError)]
        public IActionResult Post([FromForm] ShortenInputDto dto)
        {
            if (Uri.TryCreate(dto.Url, UriKind.Absolute, out Uri uriResult) &&
                (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps))
            {
                try
                {
                    return Ok(new ShortenResponseDto()
                    {
                        ShortUrl = _urlShorteningService.Shorten(uriResult),
                        OriginalUrl = uriResult.AbsoluteUri
                    });
                }
                catch(UrlAlreadyShortenException)
                {
                    throw new ApiRequestException(Errors.ALREADY_A_SHORTEN_URL);
                }
                
            }
            else
            {
                throw new ApiRequestException(Errors.INVALID_URL);
            }
        }
    }
}
