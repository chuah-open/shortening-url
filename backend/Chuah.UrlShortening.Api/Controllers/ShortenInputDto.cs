﻿using System.Diagnostics.CodeAnalysis;

namespace Chuah.UrlShortening.Api.Controllers
{
    [ExcludeFromCodeCoverage]
    public class ShortenInputDto
    {
        /// <summary>
        /// The original (long) version's URL.
        /// </summary>
        public string Url { get; set; }
    }
}
