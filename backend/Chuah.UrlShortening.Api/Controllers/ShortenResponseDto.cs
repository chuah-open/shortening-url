﻿using System.Diagnostics.CodeAnalysis;

namespace Chuah.UrlShortening.Api.Controllers
{
    [ExcludeFromCodeCoverage]
    public class ShortenResponseDto
    {
        public string ShortUrl { get; set; }

        public string OriginalUrl { get; set; }
    }
}
