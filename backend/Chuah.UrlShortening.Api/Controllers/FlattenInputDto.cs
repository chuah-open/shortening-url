﻿using System.Diagnostics.CodeAnalysis;

namespace Chuah.UrlShortening.Api.Controllers
{
    [ExcludeFromCodeCoverage]
    public class FlattenInputDto
    {
        /// <summary>
        /// The short version's URL.
        /// </summary>
        public string Url { get; set; }
    }
}
