﻿using System;
using Chuah.UrlShortening.Api.Constants;
using Chuah.UrlShortening.Api.Exceptions;
using Chuah.UrlShortening.Api.Logics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Chuah.UrlShortening.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FlattenController : ControllerBase
    {
        private readonly IUrlShorteningService _urlShorteningService;

        public FlattenController(IUrlShorteningService urlShorteningService)
        {
            _urlShorteningService = urlShorteningService;
        }

        /// <summary>
        /// Get the original URL with a shortened URL.
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(FlattenResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponseDto), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ErrorResponseDto), StatusCodes.Status500InternalServerError)]
        public IActionResult Post([FromForm] FlattenInputDto dto)
        {
            if (Uri.TryCreate(dto.Url, UriKind.Absolute, out Uri uriResult) &&
                (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps))
            {
                try
                {
                    return Ok(new FlattenResponseDto()
                    {
                        OriginalUrl = _urlShorteningService.Flatten(uriResult),
                        ShortUrl = uriResult.AbsoluteUri
                    });
                }
                catch(NotAShortenUrlException)
                {
                    throw new ApiRequestException(Errors.NOT_A_SHORTEN_URL);
                }
                
            }
            else
            {
                throw new ApiRequestException(Errors.INVALID_URL);
            }
        }
    }
}
