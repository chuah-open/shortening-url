﻿using System;

namespace Chuah.UrlShortening.Api.Logics
{
    public interface IUrlShorteningService
    {
        string Shorten(Uri uri);

        string Flatten(Uri uri);
    }
}
