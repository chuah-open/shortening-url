﻿using System;
using System.Linq;
using Chuah.UrlShortening.Api.Database;
using Chuah.UrlShortening.Api.Exceptions;

namespace Chuah.UrlShortening.Api.Logics
{
    public class UrlShorteningService : IUrlShorteningService
    {
        public const string HOST = "https://ch.ly/";
        private IInMemoryDatabase _database;

        public UrlShorteningService(IInMemoryDatabase database)
        {
            _database = database;
        }

        public string Flatten(Uri uri)
        {
            CheckUri(uri);

            if (IsShortenUrl(uri))
            {
                return _database.GetOriginalUrl(uri.AbsoluteUri);
            }
            else
            {
                throw new NotAShortenUrlException();
            }
        }

        public string Shorten(Uri uri)
        {
            CheckUri(uri);

            var existingShortenUrl = _database.GetShortenUrl(uri.AbsoluteUri);

            if (existingShortenUrl != null)
            {
                return existingShortenUrl;
            }
            else
            {
                string shortenUrl;
                do
                {
                    shortenUrl = $"{HOST}{GenerateRandomAlphanumericString()}";
                }
                while (_database.ShortenUrlExists(shortenUrl));

                _database.Add(uri.AbsoluteUri, shortenUrl);

                return shortenUrl;
            }
        }

        private void CheckUri(Uri uri)
        {
            if (uri == null)
            {
                throw new ArgumentNullException(nameof(uri));
            }

            if (uri.Scheme != Uri.UriSchemeHttp && uri.Scheme != Uri.UriSchemeHttps)
            {
                throw new ArgumentException("Invalid URI value. URI value must be either a HTTP/HTTPS format.", nameof(uri));
            }
        }

        private string GenerateRandomAlphanumericString(int length = 8)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            var random = new Random();
            var randomString = new string(Enumerable.Repeat(chars, length)
                                                    .Select(s => s[random.Next(s.Length)]).ToArray());
            return randomString;
        }

        private bool IsShortenUrl(Uri uri)
        {
            if (uri.AbsoluteUri.StartsWith(HOST))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
