﻿using System;
using System.Threading.Tasks;
using Chuah.UrlShortening.Api.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Chuah.UrlShortening.Api.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ExceptionMiddleware(
            RequestDelegate next,
            ILogger<ExceptionMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch(ApiRequestException ex)
            {
                httpContext.Response.StatusCode = StatusCodes.Status400BadRequest;

                await httpContext.Response.WriteAsJsonAsync(
                    new ErrorResponseDto()
                    {
                        Error = ex.Message
                    });
            }
            catch (Exception ex)
            {
                var errorId = Guid.NewGuid();

                _logger.LogCritical("ErrorId: {0}{1}{2}", errorId, Environment.NewLine, ex.Message);

                httpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;

                await httpContext.Response.WriteAsJsonAsync(
                    new ErrorResponseDto()
                    {
                        Error = $"Unknown error occurred. Please contact the developers with this id {errorId} if the problem persists."
                    });
            }
        }
    }
}
