﻿# About

This is a project that demonstrate a basic way to shorten a URL much like what is provided by **[bitly](https://bitly.com/)**. Basically, you can:

- Get a shortened URL for a given URL (original URL).
- Get back the URL by a shortened URL.

> **Note** 
>
> Unlike **[bitly](https://bitly.com/)**, you will always get the same shortened URL for the same original URL.



This project contains a ASP.NET Core Web API project that exposes two endpoints:

| Endpoint   | Description                                                  |
| ---------- | ------------------------------------------------------------ |
| `/shorten` | If you input  `http://www.very-long-url.com`, you will get something like `http://chuah.ly/abJkc8kZ`. |
| `/flatten` | If you input  `http://chuah.ly/abJkc8kZ`, you will get back the original URL `http://www.very-long-url.com`. |



# How to Run

Clone this repository, then build the project. Hit `F5` or `Ctrl + F5` to run. Then, navigate to [https://localhost:44388/swagger/index.html](https://localhost:44388/swagger/index.html) for the Swagger UI.

> **Note**
>
> This project is developed using .NET 5. I use Visual Studio 2019 and onwards to develop and build the code.



# References

- [How to generate a random string with C# - Jonathan Crozier](https://jonathancrozier.com/blog/how-to-generate-a-random-string-with-c-sharp)
- [ASP.NET - Use Swagger to generate API documentation (makolyte.com)](https://makolyte.com/aspnet-use-swagger-to-generate-api-documentation/)﻿



# Licensing

This is just a for fun and for learning purposes, it is free for use in anyway you like it. For example, you can clone and edit it in anyway you wish and use for your own purposes.

> **Note**
>
> Please do not send me any merge request.