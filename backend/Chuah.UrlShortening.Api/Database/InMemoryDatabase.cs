﻿using System.Collections.Generic;

namespace Chuah.UrlShortening.Api.Database
{
    public class InMemoryDatabase : IInMemoryDatabase
    {
        private readonly SortedDictionary<string, string> _dicShortenToOri;
        private readonly SortedDictionary<string, string> _dicOriToShorten;

        public InMemoryDatabase()
        {
            _dicShortenToOri = new SortedDictionary<string, string>();
            _dicOriToShorten = new SortedDictionary<string, string>();
        }

        public void Add(string originalUrl, string shortenUrl)
        {
            _dicShortenToOri.Add(shortenUrl, originalUrl);
            _dicOriToShorten.Add(originalUrl, shortenUrl);
        }

        public string GetOriginalUrl(string shortenUrl)
        {
            if (_dicShortenToOri.TryGetValue(shortenUrl, out string originalUrl))
            {
                return originalUrl;
            }
            else
            {
                return null;
            }
        }

        public string GetShortenUrl(string originalUrl)
        {
            if (_dicOriToShorten.TryGetValue(originalUrl, out string shortenUrl))
            {
                return shortenUrl;
            }
            else
            {
                return null;
            }
        }

        public bool ShortenUrlExists(string shortenUrl)
        {
            return _dicShortenToOri.ContainsKey(shortenUrl);
        }        
    }
}
