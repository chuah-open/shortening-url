﻿namespace Chuah.UrlShortening.Api.Database
{
    public interface IInMemoryDatabase
    {
        void Add(string originalUrl, string shortenUrl);

        bool ShortenUrlExists(string shortenUrl);

        string GetOriginalUrl(string shortenUrl);

        string GetShortenUrl(string originalUrl);
    }
}
