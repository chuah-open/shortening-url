﻿using System.Diagnostics.CodeAnalysis;

namespace Chuah.UrlShortening.Api
{
    [ExcludeFromCodeCoverage]
    public class ErrorResponseDto
    {
        public string Error { get; set; }
    }
}
