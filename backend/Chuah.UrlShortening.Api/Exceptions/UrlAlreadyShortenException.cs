﻿using System;

namespace Chuah.UrlShortening.Api.Exceptions
{
    public class UrlAlreadyShortenException : ApplicationException
    {
    }
}
