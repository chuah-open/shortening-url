﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Chuah.UrlShortening.Api.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class ApiRequestException : ApplicationException
    {
        public ApiRequestException(string message)
            : base(message)
        {
        }
    }
}
