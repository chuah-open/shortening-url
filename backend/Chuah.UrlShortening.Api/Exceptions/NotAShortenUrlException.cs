﻿using System;

namespace Chuah.UrlShortening.Api.Exceptions
{
    public class NotAShortenUrlException : ApplicationException
    {
    }
}
