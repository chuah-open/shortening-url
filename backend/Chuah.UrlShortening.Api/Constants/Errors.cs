﻿using System.Diagnostics.CodeAnalysis;

namespace Chuah.UrlShortening.Api.Constants
{
    [ExcludeFromCodeCoverage]
    public static class Errors
    {
        public const string ALREADY_A_SHORTEN_URL = "Already a shorten URL";
        public const string INVALID_URL = "Invalid URL";
        public const string NOT_A_SHORTEN_URL = "Not a shorten URL";
    }
}
